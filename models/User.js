const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  firstname: String,
  lastname: String,
  email: {type: 'string', unique: true},
  password: String,
  reg_time: {
    type: Date, default: Date.now
  },
  update_time: {
    type: Date, default: Date.now
  }
});

const User = mongoose.model('User', userSchema)

module.exports = User;
