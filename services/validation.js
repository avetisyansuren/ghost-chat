function validateEmail(em) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(em);
}

function validateName(name) {
    return /^[1-9 a-z]*$/.test(name);
}

function validatePass(password) {
    return /[A-Za-z0-9]{6,20}/.test(password);
}

module.exports = {
    validateEmail, validateName, validatePass
}