var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
// mongoose.Promise = require('bluebird');
var User = require('../models/User');

mongoose.connect('mongodb://localhost:27017/ghostchat', {useMongoClient: true})

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {title: 'Express'});
});

module.exports = router;
