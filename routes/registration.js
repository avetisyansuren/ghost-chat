var express = require('express');
var router = express.Router();
var md5 = require('md5');

var User = require('../models/User');

const {validateName, validateEmail, validatePass} = require('../services/validation');


router.get('/', function (req, res, next) {
    // res.send('respond with a resssssource');
    res.render('registration', {title: 'Express'});
});

router.post('/', async (req, res, next) => {

    const user_u = await User.findOne({email: req.body.email})

    if (validateEmail(req.body.email) &&
        validateName(req.body.firstname) &&
        validateName(req.body.lastname) &&
        validatePass(req.body.password) &&
        !user_u
    ) {
        var user_reg = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            password: md5(req.body.password)
        };
    } else {
        return res.render('registration', {
            error_mes: 'Incorrectly specified firstname, lastname, email or password !',
            title: 'Express'
        });
    }

    if (user_u) {
        return res.render('registration', {

            title: 'Express',
        });
    } else {
        const user = new User(user_reg)
        user.save((err, data) => {
            if (err) {
                console.log(err)
            }
        })
        return res.redirect('/login');
    }


});

module.exports = router;
