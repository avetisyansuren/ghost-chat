var express = require('express');
var router = express.Router();
var md5 = require('md5');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var User = require('../models/User');
const {validateEmail,validatePass} = require('../services/validation');


mongoose.connect('mongodb://localhost:27017/ghostchat', {useMongoClient: true})

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.render('login', {error_mes: '',})
});

router.post('/', async (req, res, next) => {

  if (validateEmail(req.body.email) && validatePass(req.body.password)) {
    var user_log = {
      email: req.body.email,
      password: md5(req.body.password)
    };
  }else {
    return res.render('login', {error_mes: 'Incorrect email or password !',});
  }


    const user_u = await User.findOne({email: user_log.email, password: user_log.password})


  if (user_u) {
    return res.redirect('/');
  } else {
    return res.render('login', {error_mes: 'Incorrect email or password !',});
  }


});

module.exports = router;
