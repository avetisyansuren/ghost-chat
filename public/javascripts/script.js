jQuery(document).ready(function ($) {
    function validateEmail(em) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(em);
    }

    function validateName(name) {
        return /^[1-9a-z]*$/ig.test(name);
    }

    function validatePass(password) {
        return /[A-Za-z0-9]{6,20}/ig.test(password);
    }

    $('#firstname').blur(function () {
        if (validateName($(this).val()) && $(this).val() != '') {
            $(this).closest('.validate-input').addClass('true-validate').removeClass('alert-validate')
        } else {
            $(this).closest('.validate-input').removeClass('true-validate').addClass('alert-validate')
        }
    })

    $('#lastname').blur(function () {
        if (validateName($(this).val()) && $(this).val() != '') {
            $(this).closest('.validate-input').addClass('true-validate').removeClass('alert-validate')
        } else {
            $(this).closest('.validate-input').removeClass('true-validate').addClass('alert-validate')
        }
    })


    $('#email').blur(function () {
        if (validateName($(this).val()) && $(this).val() != '') {
            $(this).closest('.validate-input').addClass('true-validate').removeClass('alert-validate')
        } else {
            $(this).closest('.validate-input').removeClass('true-validate').addClass('alert-validate')
        }
    })

    $('#password').blur(function () {
        if (validateName($(this).val()) && $(this).val() != '') {
            $(this).closest('.validate-input').addClass('true-validate').removeClass('alert-validate')
        } else {
            $(this).closest('.validate-input').removeClass('true-validate').addClass('alert-validate')
        }
    })


})
